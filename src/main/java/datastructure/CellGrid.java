package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
    private int rows;
    private int columns;
    private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
        //Tried firstly with arraylist, but changed after watching the walkthrough with Torstein
        if(rows <= 0 || columns <= 0) {
            throw new IllegalArgumentException();
        }
        this.rows = rows;
        this.columns = columns;
        this.grid = new CellState[rows][columns];
        for (int row = 0; row < this.rows; row++){
            for (int col = 0; col < this.columns; col++) {
                this.grid[row][col] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return this.columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        this.grid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        return this.grid[row][column];
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        CellGrid grid_copy = new CellGrid(this.rows, this.columns, null);
        for (int row = 0; row < this.rows; row++) {
            for (int column = 0; column < this.columns; column++){
                grid_copy.set(row, column, get(row, column));

            }
        }
        return grid_copy;
    }
    
}
