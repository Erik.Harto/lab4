
package cellular;

import java.util.Random;
import java.util.Objects;


import datastructure.CellGrid;
import datastructure.IGrid;

/**
 * 
 * A CellAutomata that implements Conways game of life.
 * 
 * @see CellAutomaton
 * 
 *      Every cell has two states: Alive or Dead. Each step the state of each
 *      cell is decided from its neighbors (diagonal, horizontal and lateral).
 *      If the cell has less than two alive Neighbors or more than three
 *      neighbors the cell dies. If a dead cell has exactly three neighbors it
 *      will become alive.
 * 
 * @author eivind
 * @author Martin Vatshelle - martin.vatshelle@uib.no
 * @author Sondre Bolland - sondre.bolland@uib.no
 */
public class GameOfLife implements CellAutomaton {

	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public GameOfLife(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		// TODO
		return this.currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		// TODO
		return this.currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		// TODO
		return this.currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		// TODO
		for (int row = 0; row < this.numberOfRows(); row++) {
			for (int col = 0; col < this.numberOfColumns(); col++){
				nextGeneration.set(row, col, getNextCell(row, col));
			}
		}
		this.currentGeneration = nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		// TODO
		CellState next_state = this.getCellState(row, col);
		int alive_neighbours = countNeighbors(row, col, CellState.ALIVE);
		if (Objects.equals(getCellState(row, col), CellState.ALIVE)) {
		// CellState = ALIVE{
			if (alive_neighbours < 2) {
				next_state = CellState.DEAD;
			} 
			else if (alive_neighbours > 3) {
				next_state = CellState.DEAD;
			} else {
				next_state = CellState.ALIVE;
			}
		} 
		else {
			//CellState is DEAD
			if (alive_neighbours == 3) {
				next_state = CellState.ALIVE;
			} else {
				next_state = CellState.DEAD;
			}
		}
		 
		return next_state;
	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {
		// TODO
		int numberOfNeighbours = 0;
		for (int row_index = row-1; row_index <= row+1; row_index++) {
			for (int col_index = col-1; col_index <=  col+1; col_index++){
				if (row_index < 0 || row_index >= this.numberOfRows()) {
					continue;
				} 
				if (col_index < 0 || col_index>= this.numberOfRows()){
					continue;
				} 
				if (row_index == row && col_index == col) {
					continue;
				} 
				if (Objects.equals(state, this.getCellState(row_index, col_index))) {
					numberOfNeighbours++;
				}
			}

		}
		return numberOfNeighbours;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}
